class CreatedReceiptItemDto {
  productId: number;
  receiptID: number;
  receipt: CreateReceiptDto[];
  amount: number;
}
export class CreateReceiptDto {
  customerId: number;
  employeeId: number;
  storeId: number;
  receiptItems: CreatedReceiptItemDto[];
}
