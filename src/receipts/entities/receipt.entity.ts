import { Customer } from 'src/customers/entities/customer.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Store } from 'src/store/entities/store.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Receiptdetail } from './receipt-detail';
@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  amount: number;

  @Column({ type: 'float' })
  total: number;

  @ManyToOne(() => Customer, (customer) => customer.receipts)
  customer: Customer; // Customer Id

  @ManyToOne(() => Employee, (employee) => employee.receipts)
  employee: Customer;

  @ManyToOne(() => Store, (store) => store.receipts)
  store: Store;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @OneToMany(() => Receiptdetail, (receiptdetail) => receiptdetail.receipts)
  receiptdetail: Receiptdetail[];
}
