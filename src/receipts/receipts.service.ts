import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { Receiptdetail } from './entities/receipt-detail';
import { Receipt } from './entities/receipt.entity';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt)
    private receiptsRepository: Repository<Receipt>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Receiptdetail)
    private receiptItemsRepository: Repository<Receiptdetail>,
  ) {}

  async create(createReceiptDto: CreateReceiptDto) {
    const customer = await this.customersRepository.findOneBy({
      id: createReceiptDto.customerId,
    });
    const receipt: Receipt = new Receipt();
    receipt.customer = customer;
    receipt.amount = 0;
    receipt.total = 0;
    await this.receiptsRepository.save(receipt); // ได้ id

    for (const od of createReceiptDto.receiptItems) {
      const receiptdetail = new Receiptdetail();
      receiptdetail.amount = od.amount;
      receiptdetail.product = await this.productsRepository.findOneBy({
        id: od.productId,
      });
      receiptdetail.name = receiptdetail.product.name;
      receiptdetail.price = receiptdetail.product.price;
      receiptdetail.total = receiptdetail.price * receiptdetail.amount;
      receiptdetail.receipts = receipt; // อ้างกลับ
      await this.receiptItemsRepository.save(receiptdetail);
      receipt.amount = receipt.amount + receiptdetail.amount;
      receipt.total = receipt.total + receiptdetail.total;
    }
    await this.receiptsRepository.save(receipt); // ได้ id
    return await this.receiptsRepository.findOne({
      where: { id: receipt.id },
      relations: ['receiptdetail'],
    });
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: ['customer', 'receiptdetail'],
    });
  }

  findOne(id: number) {
    return this.receiptsRepository.findOne({
      where: { id: id },
      relations: ['customer', 'receiptdetail'],
    });
  }

  update(id: number, updateReceiptDto: UpdateReceiptDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.receiptsRepository.findOneBy({ id: id });
    return this.receiptsRepository.softRemove(order);
  }
}
