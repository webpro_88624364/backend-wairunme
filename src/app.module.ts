import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { ReceiptsModule } from './receipts/receipts.module';
import { Receipt } from './receipts/entities/receipt.entity';
import { Receiptdetail } from './receipts/entities/receipt-detail';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { CategoryModule } from './category/category.module';
import { Employee } from './employees/entities/employee.entity';
import { EmployeesModule } from './employees/employees.module';
import { Category } from './category/entities/category.entity';
import { Store } from './store/entities/store.entity';
import { StoreModule } from './store/store.module';
@Module({
  imports: [
    TypeOrmModule.forRoot(
      //   {
      //   type: 'sqlite',
      //   database: 'db.sqlite',
      //   synchronize: true,
      //   migrations: [],
      //   entities: [
      //     Customer,
      //     Product,
      //     Receipt,
      //     Receiptdetail,
      //     User,
      //     Employee,
      //     Category,
      //     Store,
      //   ],
      // }
      {
        type: 'mysql',
        host: 'db4free.net',
        port: 3306,
        username: 'wairun_cafe',
        password: 'Pass@1234',
        database: 'wairun_cafe_db',
        entities: [
          Customer,
          Product,
          Receipt,
          Receiptdetail,
          User,
          Employee,
          Category,
          Store,
        ],
        synchronize: true,
      },
    ),
    CustomersModule,
    ProductsModule,
    ReceiptsModule,
    UsersModule,
    CategoryModule,
    EmployeesModule,
    StoreModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
