import { IsNotEmpty, IsEmail, Length } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  position: string;

  @IsNotEmpty()
  hourly_wage: number;
}
