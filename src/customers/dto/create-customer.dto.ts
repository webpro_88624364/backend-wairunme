import { IsNotEmpty, Length, Min } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  @Min(0)
  point: number;

  @IsNotEmpty()
  start_date: string;
}
