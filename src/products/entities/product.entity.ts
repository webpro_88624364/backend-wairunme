import { Category } from 'src/category/entities/category.entity';
import { Receiptdetail } from 'src/receipts/entities/receipt-detail';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column()
  type: string;

  @Column({ nullable: true }) //ติดไว้ก่อนนะจ๊ะค่าsizeเป็นnull////
  size: string;

  @Column({
    type: 'float',
  })
  price: number;

  @Column({
    length: '128',
    default: 'no_image_available.jpg',
  })
  image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => Receiptdetail, (receiptdetail) => receiptdetail.product)
  orderItems: Receiptdetail[];

  @ManyToOne(() => Category, (category) => category.product)
  category: Category;
}
