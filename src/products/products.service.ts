import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/category/entities/category.entity';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
  ) {}

  async create(createProductDto: CreateProductDto) {
    const category = await this.categorysRepository.findOneBy({
      id: createProductDto.categoryID,
    });
    const product = new Product();
    product.name = createProductDto.name;
    product.type = createProductDto.type;
    product.price = createProductDto.price;
    product.image = createProductDto.image;
    product.category = category;
    await this.productsRepository.save(product);
    return this.productsRepository.findOne({
      where: { id: product.id },
      relations: ['category'],
    });
  }

  findAll() {
    return this.productsRepository.find({
      relations: ['category'],
    });
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id: id },
      relations: ['category'],
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    try {
      const updatedProduct = await this.productsRepository.save({
        id,
        ...updateProductDto,
      });
      return updatedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedProduct = await this.productsRepository.remove(product);
      return deletedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
