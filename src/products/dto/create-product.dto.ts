import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';
import { CreateCategoryDto } from 'src/category/dto/create-category.dto';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  type: string;

  @IsNotEmpty()
  size: string;

  @IsNotEmpty()
  price: number;

  categoryID: number;
  categorys: CreateCategoryDto[];

  image = 'no_image_available.jpg';
}
